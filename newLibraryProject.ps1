# Global Variables

$primaryDirectories = "Configuration", "Data", "Domain", "Extensions", "Helpers", "Models", `
    "Repositories", "UnitOfWork"


# Create Project Structure
$primaryDirectories | ForEach-Object {
    mkdir $_
    Set-Location $_
    Copy-Item ../.gitkeep .
    Set-Location ..
    
}



